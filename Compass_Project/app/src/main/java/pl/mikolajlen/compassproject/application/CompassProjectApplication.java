package pl.mikolajlen.compassproject.application;

import android.app.Application;

import pl.mikolajlen.compassproject.dagger.ActivityComponent;
import pl.mikolajlen.compassproject.dagger.ActivityModule;
import pl.mikolajlen.compassproject.dagger.DaggerActivityComponent;
import pl.mikolajlen.compassproject.dagger.DaggerViewComponent;
import pl.mikolajlen.compassproject.dagger.ViewComponent;
import pl.mikolajlen.compassproject.dagger.ViewModule;
import pl.mikolajlen.compassproject.ui.activities.MainActivity;

/**
 * Created by mikolaj on 03.03.2017.
 */

public class CompassProjectApplication extends Application {

    private ViewComponent viewComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        viewComponent = DaggerViewComponent.builder()
                .viewModule(new ViewModule(this))
                .build();
    }

    public ActivityComponent getActivityComponent(MainActivity activity){
        return  DaggerActivityComponent
                .builder()
                .activityModule(new ActivityModule(this))
                .build();
    }

    public ViewComponent getViewComponent() {
        return viewComponent;
    }
}
