package pl.mikolajlen.compassproject.dagger;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.mikolajlen.compassproject.R;

/**
 * Created by mikolaj on 03.03.2017.
 */

@Module
public class ViewModule {

    private Context context;

    public ViewModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    protected Resources provideResources() {
        return context.getResources();
    }

    @Provides
    @Singleton
    protected Bitmap provideBitmap(Resources resources) {
        return BitmapFactory.decodeResource(resources, R.drawable.compass);
    }
}
