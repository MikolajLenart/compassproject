package pl.mikolajlen.compassproject.ui.view;

/**
 * Created by mikolaj on 03.03.2017.
 */

public interface RotatingView {

    void setRotation(float degrees);

    float getCurrentRotation();
}
