package pl.mikolajlen.compassproject.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.view.View;

import javax.inject.Inject;

import pl.mikolajlen.compassproject.application.CompassProjectApplication;

/**
 * Created by mikolaj on 01.03.2017.
 */

public class CompassView extends View implements RotatingView {

    private static final int DIVIDER = 2;
    private static final int MAX_ANGLE = 360;

    @Inject
    Bitmap bitmap;

    @VisibleForTesting
    float angle;

    public CompassView(Context context) {
        super(context);
        init();
    }

    public CompassView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CompassView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        CompassProjectApplication application =
                (CompassProjectApplication) getContext().getApplicationContext();
        application.getViewComponent().inject(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = resolveSize(bitmap.getWidth(), widthMeasureSpec);
        int height = resolveSize(bitmap.getHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int centerX = (canvas.getWidth() - bitmap.getWidth()) / DIVIDER;
        int centerY = (canvas.getHeight() - bitmap.getHeight()) / DIVIDER;
        canvas.rotate(angle, canvas.getWidth() / DIVIDER, canvas.getHeight() / DIVIDER);
        canvas.drawBitmap(bitmap, centerX, centerY, null);
    }

    @Override
    public void setRotation(float angle) {
        this.angle = angle % MAX_ANGLE;
        post(() -> invalidate());
    }

    @Override
    public float getCurrentRotation() {
        return angle;
    }
}
