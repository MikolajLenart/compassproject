package pl.mikolajlen.compassproject.dagger;

import dagger.Component;
import pl.mikolajlen.compassproject.ui.activities.MainActivity;

/**
 * Created by mikolaj on 03.03.2017.
 */

@Component(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);
}
