package pl.mikolajlen.compassproject.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import javax.inject.Inject;

/**
 * Created by mikolaj on 04.03.2017.
 */

public class Utils {

    @Inject
    public Utils() {
    }

    private static final String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    public void requestPermision(Activity activity, int requestCode) {

        if (!ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION) ||
                !ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, permissions, requestCode);
            return;
        }
    }

    public boolean checkIfPermissionsGranted(Activity activity, int requestCode) {
        int permissionAccessFineLocation = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionAccessCoarseLocation = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionAccessFineLocation != PackageManager.PERMISSION_GRANTED
                || permissionAccessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, permissions, requestCode);
            return true;
        }
        return false;
    }

}
