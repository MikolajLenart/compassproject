package pl.mikolajlen.compassproject.location;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import javax.inject.Inject;

import rx.subjects.PublishSubject;

/**
 * Created by mikolaj on 02.03.2017.
 */

public class LocationClient implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    private final PublishSubject<Location> locationSubject = PublishSubject.create();

    @Inject
    public LocationClient(LocationRequest locationRequest, GoogleApiClient.Builder builder) {
        this.locationRequest = locationRequest;
        googleApiClient = builder.addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        locationSubject.onCompleted();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        locationSubject.onError(new RuntimeException("Connection error"));
    }

    @Override
    public void onLocationChanged(Location location) {
        locationSubject.onNext(location);
    }

    public void connect() {
        googleApiClient.connect();
    }

    public void disconnect() {
        googleApiClient.disconnect();
    }

    public PublishSubject<Location> getLocationSubject() {
        return locationSubject;
    }
}
