package pl.mikolajlen.compassproject.ui.activities;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.mikolajlen.compassproject.R;
import pl.mikolajlen.compassproject.application.CompassProjectApplication;
import pl.mikolajlen.compassproject.location.LocationClient;
import pl.mikolajlen.compassproject.rx.AndroidTransformer;
import pl.mikolajlen.compassproject.ui.view.CompassView;
import pl.mikolajlen.compassproject.ui.view.ViewAnimator;
import pl.mikolajlen.compassproject.utils.Utils;
import rx.Subscription;

public class MainActivity extends AppCompatActivity {

    static final int REQUEST_CODE = 123;
    static final int REQUEST_CODE_START_LOCATION = 321;

    @BindView(R.id.compassView)
    CompassView compassView;

    @BindView(R.id.lattitude)
    EditText lattitude;

    @BindView(R.id.longitude)
    EditText longitude;

    @Inject
    ViewAnimator animator;

    @Inject
    LocationClient locationClient;

    @Inject
    AndroidTransformer androidTransformer;

    @Inject
    Utils utils;

    @VisibleForTesting
    Location desiredLocation;

    @VisibleForTesting
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((CompassProjectApplication)getApplicationContext())
                .getActivityComponent(this)
                .inject(this);

        utils.requestPermision(this, REQUEST_CODE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationClient.disconnect();
        if (isSubscribingLocation()) {
            subscription.unsubscribe();
        }
    }

    private boolean isSubscribingLocation() {
        return subscription != null && !subscription.isUnsubscribed();
    }


    public void setBearing(Location location) {
        if (location != null && desiredLocation != null) {
            animator.rotateTo(compassView, location.bearingTo(desiredLocation));
        }
    }

    @OnClick(R.id.startButton)
    public void startLocationListening() {
        if (!setDesiredLocation()) {
            return;
        }
        if (utils.checkIfPermissionsGranted(this, REQUEST_CODE_START_LOCATION)) {
            return;
        }
        if (!isSubscribingLocation()) {
            locationClient.connect();
            startRequestingLocationUpdates();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != REQUEST_CODE_START_LOCATION) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (permissionGranted(grantResults)) {
            startLocationListening();
        } else {
            showErrorMessage(R.string.error_permission_not_granted);
        }
    }

    private boolean permissionGranted(int[] grantResults) {
        return grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    private boolean setDesiredLocation() {
        desiredLocation = new Location("");
        try {
            desiredLocation.setLatitude(Location.convert(lattitude.getText().toString()));
            desiredLocation.setLongitude(Location.convert(longitude.getText().toString()));
            return true;
        } catch (Exception e) {
            showErrorMessage(R.string.error_invalid_input);
            if (isSubscribingLocation()) {
                subscription.unsubscribe();
            }

            return false;
        }
    }

    private void showErrorMessage(int message) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    private void startRequestingLocationUpdates() {
        subscription = locationClient.getLocationSubject()
                .compose(androidTransformer.applySchedulers())
                .subscribe(this::setBearing, throwable -> showErrorMessage(R.string.error_invalid_location));
    }
}
