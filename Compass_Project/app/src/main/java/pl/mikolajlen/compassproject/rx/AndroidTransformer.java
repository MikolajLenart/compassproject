package pl.mikolajlen.compassproject.rx;

import javax.inject.Inject;

import rx.Observable.Transformer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mikolaj on 05.01.2017.
 */

public class AndroidTransformer {

    @Inject
    public AndroidTransformer() {
    }

    public <T> Transformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
