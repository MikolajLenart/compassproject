package pl.mikolajlen.compassproject.ui.view;

import android.support.annotation.VisibleForTesting;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import pl.mikolajlen.compassproject.rx.AndroidTransformer;
import rx.Observable;
import rx.Subscription;

/**
 * Created by mikolaj on 02.03.2017.
 */

public class ViewAnimator {

    private static final int START_RANGE = 0;
    private static final int MIN_ROTATION_INTERVAL = 10;

    private AndroidTransformer androidTransformer;
    private Subscription subscription;

    @VisibleForTesting
    Observable<Integer> rotationObservable;

    @Inject
    public ViewAnimator(AndroidTransformer androidTransformer) {
        this.androidTransformer = androidTransformer;
    }

    public void rotateTo(RotatingView rotatingView, float degrees) {

        if (isCurrentlyRotatedToDesiredAngle(rotatingView, degrees) || isRotatingNow()) {
            return;
        }
        float initialAngle = rotatingView.getCurrentRotation();
        rotationObservable = Observable.zip(Observable.range(START_RANGE, (int) Math.abs(degrees - initialAngle))
                , Observable.interval(MIN_ROTATION_INTERVAL, TimeUnit.MILLISECONDS), (integer, aLong) -> integer)
                .doOnCompleted(() -> rotatingView.setRotation(degrees))
                .compose(androidTransformer.applySchedulers());
        subscription = rotationObservable
                .subscribe(integer -> rotatingView.setRotation(degrees > initialAngle
                                ? initialAngle + integer : initialAngle - integer),
                        throwable -> throwable.printStackTrace());

    }

    private boolean isRotatingNow() {
        return subscription != null && !subscription.isUnsubscribed();
    }

    private boolean isCurrentlyRotatedToDesiredAngle(RotatingView rotatingView, float degrees) {
        return degrees == rotatingView.getCurrentRotation();
    }
}
