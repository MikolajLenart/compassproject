package pl.mikolajlen.compassproject.dagger;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mikolaj on 03.03.2017.
 */

@Module
public class ActivityModule {

    private static final int MIN_INTERVAL = 1000;
    private static final int MAX_INTERVAL = 5000;
    private Context context;

    public ActivityModule(Context context) {
        this.context = context;
    }

    @Provides
    GoogleApiClient.Builder provideBuilder() {
        return new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API);
    }

    @Provides
    LocationRequest provideLocationRequest() {
        return LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setFastestInterval(MIN_INTERVAL)
                .setInterval(MAX_INTERVAL);
    }
}
