package pl.mikolajlen.compassproject.dagger;

import javax.inject.Singleton;

import dagger.Component;
import pl.mikolajlen.compassproject.ui.view.CompassView;

/**
 * Created by mikolaj on 03.03.2017.
 */

@Component(modules = ViewModule.class)
@Singleton
public interface ViewComponent {
    void inject(CompassView view);
}
