package pl.mikolajlen.compassproject.rx;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by mikolaj on 05.03.2017.
 */
public class TestTransformer extends AndroidTransformer {

    @Override
    public <T> Observable.Transformer<T, T> applySchedulers() {
        return  observable -> observable.subscribeOn(Schedulers.immediate())
                .observeOn(Schedulers.immediate());
    }
}