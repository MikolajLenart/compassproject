package pl.mikolajlen.compassproject.ui.view;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import pl.mikolajlen.compassproject.rx.TestTransformer;

import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by mikolaj on 05.03.2017.
 */
public class ViewAnimatorTest {

    private ViewAnimator viewAnimator;
    private RotatingView rotatingView;

    @Before
    public void setUp() {
        viewAnimator = new ViewAnimator(new TestTransformer());
        rotatingView = mock(RotatingView.class);
    }

    @Test
    public void shouldNotRotateViewWhenAnglesEquals() {
        //given
        given(rotatingView.getCurrentRotation()).willReturn(30.0f);

        //when
        viewAnimator.rotateTo(rotatingView, 30.0f);

        //then
        verify(rotatingView, never()).setRotation(anyFloat());
    }

    @Test
    public void shouldUpdateViewEveryDegree() {
        //given
        given(rotatingView.getCurrentRotation()).willReturn(0.0f);

        //when
        viewAnimator.rotateTo(rotatingView, 30.0f);

        //then
        Iterator<Integer> iterator = viewAnimator.rotationObservable.toBlocking().getIterator();
        for (int i = 0; i < 30; i++) {
            Assertions.assertThat(iterator.next()).isEqualTo(i);
        }
    }
}