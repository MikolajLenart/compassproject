package pl.mikolajlen.compassproject.ui.view;

import android.graphics.Canvas;
import android.support.annotation.NonNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import pl.mikolajlen.compassproject.BuildConfig;
import pl.mikolajlen.compassproject.application.CompassProjectApplicationTest;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by mikolaj on 05.03.2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, application = CompassProjectApplicationTest.class)
public class CompassViewTest {

    public static final float ANGLE = 30.0f;
    public static final int CANVAS_WIDTH = 200;
    public static final int CANVAS_HEIGHT = 100;

    @Test
    public void shouldRotateCanvas() {
        //given
        CompassView view = new CompassView(RuntimeEnvironment.application);
        view.angle = ANGLE;
        Canvas mockedCanvas = mockCanvas();

        //when
        view.onDraw(mockedCanvas);

        //then
        verify(mockedCanvas).rotate(ANGLE, CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2);
    }

    @NonNull
    private Canvas mockCanvas() {
        Canvas mockedCanvas = mock(Canvas.class);
        given(mockedCanvas.getWidth()).willReturn(CANVAS_WIDTH);
        given(mockedCanvas.getHeight()).willReturn(CANVAS_HEIGHT);
        return mockedCanvas;
    }

}