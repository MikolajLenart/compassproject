package pl.mikolajlen.compassproject.ui.activities;

import android.app.Activity;
import android.location.Location;
import android.support.annotation.NonNull;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import pl.mikolajlen.compassproject.BuildConfig;
import pl.mikolajlen.compassproject.application.CompassProjectApplicationTest;
import pl.mikolajlen.compassproject.ui.view.RotatingView;
import rx.Subscription;
import rx.subjects.PublishSubject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by mikolaj on 04.03.2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, application = CompassProjectApplicationTest.class)
public class MainActivityTest {

    MainActivity activity;

    @Before
    public void setUp() {
        activity = Robolectric.setupActivity(MainActivity.class);
    }

    @Test
    public void shouldRequestPermissionsAfterCreating() {
        verify(activity.utils).requestPermision(any(), eq(MainActivity.REQUEST_CODE));
    }

    @Test
    public void shouldUpdateViewWhenRecivedLocation() {
        //given
        PublishSubject<Location> subject = setSubject();
        setValidLocation();
        activity.startLocationListening();

        //when
        subject.onNext(new Location(""));

        //then
        verify(activity.animator).rotateTo(any(RotatingView.class), anyFloat());
    }

    @NonNull
    private PublishSubject<Location> setSubject() {
        PublishSubject<Location> subject = PublishSubject.create();
        given(activity.locationClient.getLocationSubject()).willReturn(subject);
        return subject;
    }

    private void setValidLocation() {
        activity.lattitude.setText("51.22");
        activity.longitude.setText("22.22");
    }

    @Test
    public void shouldStartListeningToLocation() {
        //given
        PublishSubject<Location> subject = setSubject();
        setValidLocation();
        given(activity.utils.checkIfPermissionsGranted(any(Activity.class), anyInt())).willReturn(false);

        //when
        activity.startLocationListening();

        //then
        verify(activity.locationClient).connect();
    }

    @Test
    public void shouldSetDesiredLocation() {
        //given
        setValidLocation();
        mockSubscription();

        //when
        activity.startLocationListening();

        //then
        Assertions.assertThat(activity.desiredLocation.getLatitude()).isEqualTo(51.22);
        Assertions.assertThat(activity.desiredLocation.getLongitude()).isEqualTo(22.22);
    }

    private void mockSubscription() {
        Subscription subscription = mock(Subscription.class);
        given(subscription.isUnsubscribed()).willReturn(false);
        activity.subscription = subscription;
    }

    @Test
    public void shouldRequestPermissionsOnButtonClick() {
        //given
        setValidLocation();
        mockSubscription();

        //when
        activity.startLocationListening();

        //then
        verify(activity.utils).checkIfPermissionsGranted(activity, MainActivity.REQUEST_CODE_START_LOCATION);
    }
}