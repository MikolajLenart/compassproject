package pl.mikolajlen.compassproject.application;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.mikolajlen.compassproject.location.LocationClient;
import pl.mikolajlen.compassproject.rx.AndroidTransformer;
import pl.mikolajlen.compassproject.rx.TestTransformer;
import pl.mikolajlen.compassproject.ui.view.ViewAnimator;
import pl.mikolajlen.compassproject.utils.Utils;

import static org.mockito.Mockito.mock;

/**
 * Created by mikolaj on 04.03.2017.
 */

@Module
public class TestActivityModule {

    @Provides
    @Singleton
    GoogleApiClient.Builder provideBuilder() {
        return mock(GoogleApiClient.Builder.class);
    }

    @Provides
    @Singleton
    LocationRequest provideLocationRequest() {
        return mock(LocationRequest.class);
    }

    @Provides
    @Singleton
    ViewAnimator provideViewAnimator() {
        return mock(ViewAnimator.class);
    }

    @Provides
    @Singleton
    LocationClient provideLocationClient() {
        return mock(LocationClient.class);
    }

    @Provides
    @Singleton
    Utils provdeUtils(){
        return mock(Utils.class);
    }

    @Provides
    @Singleton
    AndroidTransformer provideAndroidTransformer(){
        return new TestTransformer();
    }
}