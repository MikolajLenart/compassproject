package pl.mikolajlen.compassproject.application;

import pl.mikolajlen.compassproject.dagger.ActivityComponent;
import pl.mikolajlen.compassproject.ui.activities.MainActivity;

/**
 * Created by mikolaj on 04.03.2017.
 */
public class CompassProjectApplicationTest extends CompassProjectApplication{

    @Override
    public ActivityComponent getActivityComponent(MainActivity activity) {
        return DaggerTestActivityComponent
                .builder()
                .testActivityModule(new TestActivityModule())
                .build();
    }
}