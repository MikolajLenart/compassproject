package pl.mikolajlen.compassproject.application;

import javax.inject.Singleton;

import dagger.Component;
import pl.mikolajlen.compassproject.dagger.ActivityComponent;
import pl.mikolajlen.compassproject.ui.activities.MainActivity;

/**
 * Created by mikolaj on 04.03.2017.
 */

@Component(modules = TestActivityModule.class)
@Singleton
public interface TestActivityComponent extends ActivityComponent {

    void inject(MainActivity mainActivity);
}